<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/profile', 'User\UserController@profile')->name('profile');    //profile

Route::post('/profile/edit', 'User\Usercontroller@editUser')->name('user.edit');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/order', 'orderController@orderPage')->name('order.page');
Auth::routes();
Route::get('/index', 'IndexController@index')->name('index');
Route::get('/home', 'IndexController@index')->name('home');
Route::get('/logouts', 'UserController@logouts')->name('logouts');
Route::post('/order', 'orderController@createOrder')->name('order.create');
Route::post('/sendmessage', 'oneChatController@sendMessage')->name('onechat.sendmessage');
Route::post('/checkuser', 'oneChatController@checkUser')->name('onechat.checkuser');
