<?php

use App\Product;
use Illuminate\Database\Seeder;
 

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(ProductSeeder::class);
        $datas = [
            [
                'shirt_code' => 'A310',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'AQ',
                'price' => 200,
                'imgUrl' => 'http://www.flyhawksport.com//images/product/A310-LG1-thumb.jpg'    
            ],
            [
                'shirt_code' => 'A311',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'MD',
                'price' => 200,
                'imgUrl' => 'http://www.flyhawksport.com//images/product/A322-G-thumb(2).jpg' 
            ],
            [
                'shirt_code' => 'A312',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'TTC',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A333-O-thumb(2).jpg'   
            ],
            [
                'shirt_code' => 'A313',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'TTC',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A333-O-thumb(2).jpg'   
            ],
            [
                'shirt_code' => 'A314',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'TC',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A348-LG-thumb.jpg'   
            ],
            [
                'shirt_code' => 'A315',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'AQ',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A349-O-thumb.jpg'   
            ],
            [
                'shirt_code' => 'A316',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'MD',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A352-Y-thumb.jpg'   
            ],
            [
                'shirt_code' => 'A317',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'TTC',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A353-R-thumb.jpg'   
            ],
            [
                'shirt_code' => 'A318',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'TTC',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A355-YG-thumb.jpg'   
            ],
            [
                'shirt_code' => 'A319',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'TC',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A369-lo-thumb.jpg'   
            ],
            [
                'shirt_code' => 'A320',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'material' => 'AQ',
                'price' => 300,
                'imgUrl' =>  'http://www.flyhawksport.com//images/product/A379_W_thumb.jpg'   
            ]
            
        ];

        foreach( $datas as $item){
            Product::create($item);
        }
    }
}
