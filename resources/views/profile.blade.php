@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                      <img src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F1162153495%2F960x0.jpg%3Ffit%3Dscale" class="d-block w-100" alt="employee image">
                  </div>
                  <div class="carousel-item">
                    <img src="https://s3.amazonaws.com/media.eremedia.com/wp-content/uploads/sites/4/2019/01/14105557/onboarding-welcome-new-700x467.jpg" class="d-block w-100" alt="nigga image">
                  </div>
                  <div class="carousel-item">
                    <img src="https://s17026.pcdn.co/wp-content/uploads/sites/9/2017/07/Employee-26717.jpeg" class="d-block w-100" alt="image">
                  </div>
                </div>
              </div></div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Profile</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="#" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" value="{{ $user->name }}" id="name" readonly>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" value="{{ $user->email }}" id="email" readonly>
                            </div>
                        </form>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit-profile">
                            Edit
                          </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit-profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form role="form" action="{{ route('user.edit') }} " method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" class="form-control" name="username" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
@endsection