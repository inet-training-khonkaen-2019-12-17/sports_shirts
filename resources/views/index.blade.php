@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('showAlertLoginFirst'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
                {{session('showAlertLoginFirst')}}
            </div>
        </div>
    </div>
    @endif
    @if (session('showAlertCanNotAct'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
                {{session('showAlertCanNotAct')}}
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        @foreach($products as $item)
        <div class="col-sm-4">
            <div class="card-columns-fluid">
                <div class="card  bg-light mb-4" style = "max-width: 22rem; box-shadow: 1px 8px 20px grey; ">
                    <img class="card-img-top" src="{{$item->imgUrl}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$item->shirt_code}}</h5>
                        <h5 class="card-title">{{$item->shirt_name}}</h5>
                        <p class="card-text">{{$item->detail}}</p>
                        <form action="{{ route('order.create')}}" method="post">
                            @csrf
                            <input type ="hidden" style = "width: 20rem; " value="B0001" name="order_id" id="order_id">
                            <input type ="hidden" style = "width: 20rem; " value="{{$item->shirt_code}}" name="shirt_id" id="shirt_id">
                            {{-- <input type ="hidden" style = "width: 20rem; " value="{{Auth::user()->email}}" name="user_mail" id="user_mail"> --}}
                            <input type ="hidden" style = "width: 20rem; " value="{{$item->price}}" name="price" id="price">
                            <input class="btn btn-primary" type ="submit" style = "width: 20rem; " value="{{$item->price}}" >
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
