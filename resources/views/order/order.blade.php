@extends('layouts.app') @section('content') {{-- <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>SPORTS SHIRT STORE</title>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet"
        />

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css?family=Nunito"
            rel="stylesheet"
        />

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    </head>
    --}} {{--
    <body class="container-md">
        --}}
        <div class="container-md">
            {{--
            <form action="{{ route('order.create') }}" id="form" method="POST">
                @csrf
                <input type="hidden" value="B0001" name="order_id" />
                <input type="hidden" value="A314" name="shirt_id" />
                <input type="hidden" value="U0001" name="user_id" />
                <input type="hidden" value="200" name="price" />
                <input type="submit" class="btn btn-primary" value="FAKE" />
            </form>
            --}}
            <div class="card">
                <div class="card-header">รายการสั่งซื้อ</div>
                <div class="list-group list-group-flush">
                    @foreach ($order as$index => $item)
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <img
                                    src="{{$item->product->imgUrl}}"
                                    alt="shirt{{$item->product->imgUrl}}"
                                />
                            </div>
                            <div class="col-md-6">
                                <h5>
                                    รายการสั่งซื้อหมายเลข: {{$item->order_id}}
                                </h5>
                                <h5>สั่งเสื้อหมายเลข: {{$item->shirt_id}}</h5>
                                <h5>ราคา: {{$item->price}}</h5>
                                <h5>สถานะ:
                                    @if ($item->status === 1)
                                    <span class="badge badge-success">ชำระเรียบร้อย</span>
                                    @elseif ($item->status === 0)
                                    <span class="badge badge-warning">ยังไม่ชำระ</span>
                                    @else
                                    <span class="badge badge-danger">สถานะการชำระไม่ถูกต้อง</span>
                                    @endif
                                </h5>
                                <p class="pt-4">
                                    ชื่อผู้ใช้ที่สั่งสินค้า: {{$item->user_mail}}
                                </p>
                            </div>
                        </div>
                            <div class="row justify-content-end">
                                <div class="col-md-2 text-md-right">
                                {{-- <form action="{{route('onechat.sendmessage', $item->order_id)}}" method="post"> --}}
                                    {{-- @csrf --}}
                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">ชำระเงิน</button>
                                {{-- </form> --}}
                                </div>
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- modal -->
            <div
                class="modal fade"
                id="exampleModal"
                tabindex="-1"
                role="dialog"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
            >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                การชำระสินค้า
                            </h5>
                            <button
                                type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('onechat.checkuser')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="onechatId" class="lead">One Chat ID</label>
                                    </div>
                                    @if (session('CheckOneId'))
                                        <div class="alert alert-danger" role="alert">
                                            {{session('CheckOneId')}}
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <input class="form-control" type="text" id="oneAcc" name="oneAcc" required>
                                    </div>
                                    <div class="col-md-12">
                                        <small id="oneIdHelp" class="form-text text-muted">One mail, Phone number, or One ID ของ One Chat</small>
                                    </div>
                                </div>
                                <div class="row justify-content-md-end">
                                    <div class="col-md-6 text-md-right">
                                        <input type="submit" class="btn btn-primary mt-2" value="ส่งข้อมูลขั้นตอนการชำระเงิน">
                                    </div>
                                </div>
                            </form>
                            {{-- <h4>ขั้นตอนการชำระเงินถูกส่งไปที่ OneChat ของท่านแล้วกรุณาตรวจสอบ</h4> --}}
                        </div>
                        <div class="modal-footer">
                            <button
                                type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal"
                            >
                            ปิด
                            </button>
                            {{--
                            <button type="button" class="btn btn-primary">
                                ชำระสินค้า
                            </button>
                            --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--
    </body>
</html>
--}} @endsection
