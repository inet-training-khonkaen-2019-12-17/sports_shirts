<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class productController extends Controller
{
    public function create(Request $request)
    {
        // $product = new Product();
        // return Product::created($request);
        // DB::table('shirt_store')->insert([
        //     ['email' => 'taylor@example.com', 'votes' => 0],
        //     ['email' => 'dayle@example.com', 'votes' => 0]
        // ]);


        // if (!$book->save()) {
        //     return redirect()->route('book.create.page');
        // }

        // return redirect()->route('book.page');
        $datas = [
            [
                'shirt_code' => 'A310',
                'shirt_name' => 'เสื้อโปโล สุภาพบุรุษ',
                'detail' => 'มีกระเป๋าหน้าอก ใส่เล่นกีฬาหรือใส่เที่ยว ผ้าสวมใส่สบาย น้ำหนักเบา ไม่อุ้มน้ำ เนื้อผ้าระบายอากาศได้ดี',
                'size' => 'Free Size',
                'Material' => 'AQ',
                'price' => '100'     
            ]
        ];

        foreach( $datas as $item){
            Product::create($item);
        }
        return "yes";
    }
}
