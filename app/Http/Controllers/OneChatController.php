<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Onechat;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class OneChatController extends Controller
{
    public function checkUser(Request $request)
    {
        $info = $this->checkOneChatUser($request->oneAcc);
        // dd($info);
        if ($info->status === 'fail') {
            return redirect()->back()->with(['status' => 'Fail!']);
        }

        // $onechat = new Onechat();
        // $onechat->one_mail = $request->one_mail;
        // $onechat->onechat_id = $info->friend->user_id;
        // $onechat->user_id = Auth::user()->id;

        // if (!$onechat->save()) {
        //     return redirect()->back()->with(['status' => 'Save Fail!']);
        // }
        $this->sendMessage('สวัสดี คุณได้สั่งสินค้าเรียบร้อยแล้ว',  $info->friend->user_id);
        return redirect()->back();
    }

    private function checkOneChatUser($email)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-manage.one.th:8997/api/v1/searchfriend", [
                "headers" => [
                    'Authorization' => "Bearer A34803f96484d53d78b35958751901d41d42f0558281f4683919d8f6a44c93ecf5212cb58351c409dacf4bf4afff866d7",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'bot_id' => "Bfe703e70cd1f579f9a53ed21a229cb92",
                    "key_search" => $email
                ]
            ]);
            $resToJson = json_decode($res->getBody()->getContents());
            return $resToJson;
        } catch (GuzzleException $e) {
           return (object) ['status' => 'fail'];
        }
    }

    private function sendMessage($msg, $onechat_id)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-public.one.th:8034/api/v1/push_message", [
                "headers" => [
                    'Authorization' => "Bearer A34803f96484d53d78b35958751901d41d42f0558281f4683919d8f6a44c93ecf5212cb58351c409dacf4bf4afff866d7",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'to' => $onechat_id,
                    'bot_id' => "Bfe703e70cd1f579f9a53ed21a229cb92",
                    'type' => 'text',
                    "message" => $msg,
                ]
            ]);
            return null;
        } catch (GuzzleException $e) {
            return null;
        }
    }
}
