<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        // dd($user);
        return view('profile', compact('user'));
    }

    public function editUser(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->username;
        $user->email = $request->email;
        $user->save();
        return redirect()->route('profile');
        // if (!$user->save()) {
        //     dd(1);
        //     return redirect()->route('profile');
        // }


        // return user::update(["username"=>])
    }

    public function logouts()
    {
        Auth::logout();
        return redirect('/home');
    }

}
