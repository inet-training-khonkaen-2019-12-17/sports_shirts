<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function orderPage()
    {
        $order = Order::with('product')->get();
        // $shirtPic = ;
        // // dd($order->all());
        // foreach ($order as $key => $value) {
        //     $temp = $value->shirt_id;
        // }
        // $shirtPic = Product::where('shirt_code', $temp);
        // dd($order[0]->product);
        return view('order.order', ['order' => $order]);
        // return view('order.order');
    }

    public function createOrder(Request $request)
    {
        if(!Auth::user()) {
            // dd($request->all());
            return redirect()->back()->with(['showAlertLoginFirst' => 'กรุณาลงชื่อเข้าสู่ระบบเพื่อทำการสั่งซื้อสินค้า']);
        }
        // dd($request->order_id);
        $order = new Order();
        $order->order_id = $request->order_id;
        $order->shirt_id = $request->shirt_id;
        $order->user_mail = Auth::user()->email;
        $order->price = $request->price;
        $order->status = 0;
        // dd($order);
        if(!$order->save()) {
            return redirect()->back()->with(['showAlertCanNotAct' => 'เกิดข้อผิดพลาดไม่สามารถสั่งซื้อสินค้าได้']);
        }
        // return redirect(route('order.page'));
        // $order = Order::create($request->all());
        return redirect(route('order.page'));
    }
}
