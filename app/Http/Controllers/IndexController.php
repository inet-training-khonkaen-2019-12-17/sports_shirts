<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
       // return view('index');
        $products = Product::all();
       
        
        return view('index', compact('products'));
    }

    // function indexPage()
    // {
        
    //     $products = Product::all();
    //     dd($products);
        
    //     return view('index', compact('products'));
    // }
}
