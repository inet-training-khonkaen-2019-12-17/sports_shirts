<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['shirt_code','shirt_name','detail','size','material','price','imgUrl'];
}
