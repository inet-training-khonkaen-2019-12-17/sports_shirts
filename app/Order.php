<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['order_id','shirt_id','user_mail','price','status'];

    public function product()
    {
        // dd(1);
        return $this->hasOne(Product::class, 'shirt_code', 'shirt_id');
    }

}
